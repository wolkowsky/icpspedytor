﻿namespace Spedytor
{
    partial class Spedytor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GLS = new System.Windows.Forms.Button();
            this.btnFedex = new System.Windows.Forms.Button();
            this.btnPocztaPolska = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnDPD = new System.Windows.Forms.Button();
            this.dpdProtokolPrzekazania = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // GLS
            // 
            this.GLS.Location = new System.Drawing.Point(13, 84);
            this.GLS.Name = "GLS";
            this.GLS.Size = new System.Drawing.Size(190, 38);
            this.GLS.TabIndex = 0;
            this.GLS.Text = "GLS";
            this.GLS.UseVisualStyleBackColor = true;
            this.GLS.Click += new System.EventHandler(this.gls_Click);
            // 
            // btnFedex
            // 
            this.btnFedex.Location = new System.Drawing.Point(12, 128);
            this.btnFedex.Name = "btnFedex";
            this.btnFedex.Size = new System.Drawing.Size(190, 38);
            this.btnFedex.TabIndex = 1;
            this.btnFedex.Text = "FedEx";
            this.btnFedex.UseVisualStyleBackColor = true;
            this.btnFedex.Click += new System.EventHandler(this.btnFedex_Click);
            // 
            // btnPocztaPolska
            // 
            this.btnPocztaPolska.Location = new System.Drawing.Point(13, 172);
            this.btnPocztaPolska.Name = "btnPocztaPolska";
            this.btnPocztaPolska.Size = new System.Drawing.Size(190, 38);
            this.btnPocztaPolska.TabIndex = 2;
            this.btnPocztaPolska.Text = "Poczta Polska";
            this.btnPocztaPolska.UseVisualStyleBackColor = true;
            this.btnPocztaPolska.Click += new System.EventHandler(this.btnPocztaPolska_Click);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(13, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(385, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnDPD
            // 
            this.btnDPD.Location = new System.Drawing.Point(12, 40);
            this.btnDPD.Name = "btnDPD";
            this.btnDPD.Size = new System.Drawing.Size(190, 38);
            this.btnDPD.TabIndex = 4;
            this.btnDPD.Text = "DPD";
            this.btnDPD.UseVisualStyleBackColor = true;
            this.btnDPD.Click += new System.EventHandler(this.btnDPD_Click);
            // 
            // dpdProtokolPrzekazania
            // 
            this.dpdProtokolPrzekazania.Location = new System.Drawing.Point(208, 40);
            this.dpdProtokolPrzekazania.Name = "dpdProtokolPrzekazania";
            this.dpdProtokolPrzekazania.Size = new System.Drawing.Size(190, 38);
            this.dpdProtokolPrzekazania.TabIndex = 5;
            this.dpdProtokolPrzekazania.Text = "DPD - Generowanie protokołu przekazania";
            this.dpdProtokolPrzekazania.UseVisualStyleBackColor = true;
            this.dpdProtokolPrzekazania.Click += new System.EventHandler(this.dpdProtokolPrzekazania_Click);
            // 
            // Spedytor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 224);
            this.Controls.Add(this.dpdProtokolPrzekazania);
            this.Controls.Add(this.btnDPD);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnPocztaPolska);
            this.Controls.Add(this.btnFedex);
            this.Controls.Add(this.GLS);
            this.Name = "Spedytor";
            this.Text = "Spedytor";
            this.Load += new System.EventHandler(this.Spedytor_Load);
            this.Shown += new System.EventHandler(this.Spedytor_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GLS;
        private System.Windows.Forms.Button btnFedex;
        private System.Windows.Forms.Button btnPocztaPolska;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnDPD;
        private System.Windows.Forms.Button dpdProtokolPrzekazania;
    }
}

