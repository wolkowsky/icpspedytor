﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spedytor
{
    public static class GlobalStorage
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["connStr"].ConnectionString;
        public static readonly string FolderDokumentow = ConfigurationManager.AppSettings["FolderDokumentow"];
    }
}
