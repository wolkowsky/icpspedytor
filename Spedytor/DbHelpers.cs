﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SmartWysylka;

namespace Spedytor
{
    public static class DbHelpers
    {
        public static bool CheckIfTableExists()
        {
            const string sqlSelect = @"IF (EXISTS (SELECT * 
                                 FROM INFORMATION_SCHEMA.TABLES 
                                 WHERE TABLE_SCHEMA = 'CDN' 
                                 AND  TABLE_NAME = 'NXSpedycja'))
                                    SELECT 1 as 'Exists'
                                ELSE
	                                SELECT 0 as 'Exists'";
            try
            {
                using (SqlConnection conn = new SqlConnection(GlobalStorage.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sqlSelect, conn))
                    {
                        conn.Open();
                        return (int)cmd.ExecuteScalar() == 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Błąd danych wejściowych.  Dodatkowe informacje: {ex.Message}");
            }
        }

        public static void CreateTableForSpedition()
        {
            const string sqlSelect = @"CREATE TABLE CDN.NXSpedycja
                                (
	                                Spedytor varchar(20),
	                                NumerListu varchar(40),
                                    IdPrzesylki varchar(50),	
	                                DataWygenerowaniaPaczki datetime,
	                                CzyNaProtokolePrzekazania bit,
	                                DataWysylki datetime,
	                                Opis varchar(max),
                                    GidTyp int,
                                    GidNumer int
                                )";
            try
            {
                using (SqlConnection conn = new SqlConnection(GlobalStorage.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sqlSelect, conn))
                    {
                        conn.Open();
                        cmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Nie udało się utworzy tabeli. Dodatkowe informacje: {ex.Message}");
            }
        }

        public static void InsertPackage(WynikWysylki ww, string spedytor, int gidTyp, int gidNumer, string opis = "")
        {
            const string sqlSelect = @"INSERT INTO CDN.NXSpedycja (Spedytor, NumerListu, IdPrzesylki, DataWygenerowaniaPaczki, CzyNaProtokolePrzekazania, Opis, GidTyp, GidNumer)
                                        VALUES(@spedytor, @numerListu, @idPrzesylki, @dataWygenerowania, 0, @opis, @gidTyp, @gidNumer)";
            try
            {
                using (SqlConnection conn = new SqlConnection(GlobalStorage.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sqlSelect, conn))
                    {
                        cmd.Parameters.AddWithValue("@spedytor", spedytor);
                        cmd.Parameters.AddWithValue("@numerListu", ww.NumerWysylki);
                        cmd.Parameters.AddWithValue("@idPrzesylki", ww.IdPrzesylki);
                        cmd.Parameters.AddWithValue("@dataWygenerowania", DateTime.Now);
                        cmd.Parameters.AddWithValue("@opis", opis);
                        cmd.Parameters.AddWithValue("@gidTyp", gidTyp);
                        cmd.Parameters.AddWithValue("@gidNumer", gidNumer);

                        conn.Open();
                        cmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Nie udało się utworzy tabeli. Dodatkowe informacje: {ex.Message}");
            }
        }

        public static List<string> GetPackageId(string spedytor)
        {
            List<string> listNumbers = new List<string>();

            const string sqlSelect = @"SELECT IdPrzesylki FROM CDN.NXSpedycja WHERE Spedytor = @spedytor AND CzyNaProtokolePrzekazania = 0";
            try
            {
                using (SqlConnection conn = new SqlConnection(GlobalStorage.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sqlSelect, conn))
                    {
                        cmd.Parameters.AddWithValue("@spedytor", spedytor);

                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    listNumbers.Add(reader["IdPrzesylki"].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Wystąpił błąd: {ex.Message}");
            }

            return listNumbers;
        }

        public static void UpdateListNumbers(string spedytor)
        {
            const string sqlSelect = @"UPDATE CDN.NXSpedycja 
                                        SET CzyNaProtokolePrzekazania = 1, DataWysylki = @dataWysylki
                                        WHERE Spedytor = @spedytor AND CzyNaProtokolePrzekazania = 0";
            try
            {
                using (SqlConnection conn = new SqlConnection(GlobalStorage.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(sqlSelect, conn))
                    {
                        cmd.Parameters.AddWithValue("@spedytor", spedytor);
                        cmd.Parameters.AddWithValue("@dataWysylki", DateTime.Now);

                        conn.Open();

                        cmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Nie udało się utworzy tabeli. Dodatkowe informacje: {ex.Message}");
            }
        }
    }
}
