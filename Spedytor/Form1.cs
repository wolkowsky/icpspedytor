﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using SmartWysylka;
using SmartWysylka.Specs;
using TaskProcessor.Plugin.ERPXL;
using TaskProcessor.Plugin.ERPXL.Common;
using Enums = SmartWysylka.Enums;

namespace Spedytor
{
    public partial class Spedytor : Form
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["connStr"].ConnectionString;

        private readonly string _ope = ConfigurationManager.AppSettings["operator"];
        private readonly string _pass = ConfigurationManager.AppSettings["haslo"];
        private readonly string _baza = ConfigurationManager.AppSettings["baza"];
        private readonly string _nazwaAtrybuty = ConfigurationManager.AppSettings["NazwaAtrybutu"];

        private bool _glsEnable = Boolean.Parse(ConfigurationManager.AppSettings["GLSEnable"]);
        private bool _fedexEnable = Boolean.Parse(ConfigurationManager.AppSettings["FedExEnable"]);
        private bool _pocztaPolskaEnable = Boolean.Parse(ConfigurationManager.AppSettings["PocztaPolskaEnable"]);
        private bool _DPDEnable = Boolean.Parse(ConfigurationManager.AppSettings["DPDEnable"]);


        private string[] args;

        private XLDokHandlowyNag dokHandlowy = new XLDokHandlowyNag();
        private XLKontrahentAdres kntAdres = new XLKontrahentAdres();
        private List<XLAtrybut> _atr = null;
        private static int _gidNumer = 0;
        private static int _gidTyp = 0;


        public Spedytor()
        {
            try
            {
                //args = Environment.GetCommandLineArgs();
                //WczytajParamXL(args);
                int _gidTyp = 2033;
                int _gidNumer = 2833;



                if (_gidNumer == 0)
                {
                    //MessageBox.Show(@"Błędny parametr wejściowy");
                    //Environment.Exit((int)ExitCode.BlednyParametrWejsciowy);
                    _glsEnable = false;
                    _fedexEnable = false;
                    _pocztaPolskaEnable = false;
                    _DPDEnable = false;
                }
                else
                {
                    dokHandlowy = Plugin.DokHandlowyWczytaj(_gidNumer, _connStr);

                    _atr = Plugin.AtrybutWczytajListe(new XLAtrybut() { GIDNumer = _gidNumer, GIDTyp = _gidTyp }, _connStr);

                    if (_atr.Count(x => x.Klasa == _nazwaAtrybuty) > 0)
                    {
                        if (string.IsNullOrEmpty(_atr.Single(x => x.Klasa == _nazwaAtrybuty).Wartosc))
                        {
                            kntAdres = dokHandlowy.AdwNumer != null ? Plugin.KontrahentAdresWczytaj((int)dokHandlowy.AdwNumer, _connStr) : Plugin.KontrahentAdresWczytaj((int)dokHandlowy.AdrNumer, _connStr);
                        }
                        else
                        {
                            MessageBox.Show(@"Zamówienie ma nadany numer przesyłki. Brak możliwości ponownego nadania. Tryb generowania protokołu przekazania.");
                            _glsEnable = false;
                            _fedexEnable = false;
                            _pocztaPolskaEnable = false;
                            _DPDEnable = false;
                        }
                    }
                    else
                    {
                        kntAdres = dokHandlowy.AdwNumer != null ? Plugin.KontrahentAdresWczytaj((int)dokHandlowy.AdwNumer, _connStr) : Plugin.KontrahentAdresWczytaj((int)dokHandlowy.AdrNumer, _connStr);
                    }
                }

                InitializeComponent();

                textBox1.Text = PobierzNumerDok(_gidTyp, _gidNumer);
                GLS.Enabled = _glsEnable;
                btnFedex.Enabled = _fedexEnable;
                btnPocztaPolska.Enabled = _pocztaPolskaEnable;
                btnDPD.Enabled = _DPDEnable;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Błąd: {ex.Message}, {ex.InnerException}");
            }
        }

        private void gls_Click(object sender, EventArgs e)
        {
            var nc = new NetworkCredential(
                ConfigurationManager.AppSettings["SpedytorGLSUzytkownik"],
                ConfigurationManager.AppSettings["SpedytorGLSHaslo"],
                ConfigurationManager.AppSettings["SpedytorGLSApiAdres"]
                );

            DanePrzesylki dp = UstawDaneWysylki();
            var wynikWysylki = Helpers.GenerujDaneWysylkiZPytaniemOParametryWin(Enums.TypPrzesylki.GlsPaczka, dp, nc);

            if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.Ok)
            {
                UstawAtr(wynikWysylki.NumerWysylki);
                PokazInfoIZakonczPrace(wynikWysylki);
            }
            else if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.CancelByUser)
            {
                MessageBox.Show(@"Anulowano");
            }
            else
            {
                MessageBox.Show(String.Format("Wystąpił błąd o treści: {0}, Kod błędu: {1}", wynikWysylki.Info,
                    wynikWysylki));
            }
        }

        private DanePrzesylki UstawDaneWysylki()
        {
            var danePrzes = new DanePrzesylki()
            {
                Nadawca = new DaneKontrahenta()
                {
                    NazwaFirmy = ConfigurationManager.AppSettings["NazwaFirmy"],
                    NumerKonta = ConfigurationManager.AppSettings["NumerKontaBankowego"],
                    KodKraju = "PL",
                    KodPocztowy = ConfigurationManager.AppSettings["KodPocztowy"],
                    Miasto = ConfigurationManager.AppSettings["Miasto"],
                    Ulica = ConfigurationManager.AppSettings["Ulica"],
                    NumerDomu = ConfigurationManager.AppSettings["NumerDomu"],
                    Nazwisko = ConfigurationManager.AppSettings["Nazwisko"]
                    //NumerKlienta = int.Parse(ConfigurationManager.AppSettings["SpedytorDPDFID"])
                },
                Odbiorca = new DaneKontrahenta()
                {
                    NazwaFirmy = kntAdres.Nazwa1 + (!string.IsNullOrEmpty(kntAdres.Nazwa2) ? " " + kntAdres.Nazwa2 : ""),
                    Email = kntAdres.EMail,
                    KodKraju = "PL",
                    Miasto = kntAdres.Miasto,
                    KodPocztowy = kntAdres.KodP,
                    Ulica = kntAdres.Ulica,
                    TelKontaktowy = kntAdres.Telefon1,
                    NumerNipFirmy = kntAdres.NipE,
                    NumerDomu = " ",
                    Imie = "",
                    //Nazwisko = zam.AdresWysylkowy.Nazwa1 + (!string.IsNullOrEmpty(zam.AdresWysylkowy.Nazwa2) ? " " + zam.AdresWysylkowy.Nazwa2 : ""),
                }
            };

            return danePrzes;
        }

        private static void WczytajParamXL(string[] args)
        {
            int ret = 0;

            var xlArgs = File.ReadAllLines(args[1]);
            foreach (var s in xlArgs)
            {
                var x = s.Replace(",", ";").Split(';');
                _gidTyp = Int32.Parse(x[0]);
                _gidNumer = Int32.Parse(x[1]);
            }
        }

        public static void PokazPDFListuPrzewozowego(string nrPrzesylki, byte[] bytesPDF)
        {
            try
            {
                string fileName = string.Format("{0}_({1}_{2}).pdf", nrPrzesylki, DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()).Replace(" ", "_").Replace(":", "");
                string fileFullPath = Path.Combine(GlobalStorage.FolderDokumentow, fileName);
                //string fileFullPath = Path.Combine(Path.GetTempPath(), fileName);

                using (var fileStream = new FileStream(fileFullPath, FileMode.OpenOrCreate))
                {
                    fileStream.Write(bytesPDF, 0, bytesPDF.Length);
                }

                Process.Start(fileFullPath);
            }
            catch
            {
                //MessageBox.Show(ex.Message);
            }
        }

        enum ExitCode : int
        {
            Success = 0,
            BlednyParametrWejsciowy = 1,
            ZamowienieZNadanymNumeremPrzesylki = 2,
        }

        private string PobierzNumerDok(int gidTyp, int gidNumer)
        {
            return Plugin.NarzedziaPobierzNumerDok(gidTyp, gidNumer, _connStr);
        }
        public void UstawAtr(string NumerWysylki)
        {
            int idSesji = XlZaloguj();
            XLAtrybut atr = null;

            if (_atr.Count(x => x.Klasa == _nazwaAtrybuty) > 0)
            {
                atr = _atr.Single(x => x.Klasa == _nazwaAtrybuty);
            }

            try
            {
                if (atr != null)
                {
                    atr.Wartosc = NumerWysylki;
                    Plugin.AtrybutAktualizuj(idSesji, atr, _connStr);
                }
                else
                {
                    atr = new XLAtrybut();
                    atr.Klasa = _nazwaAtrybuty;
                    atr.Wartosc = NumerWysylki;
                    atr.GIDNumer = _gidNumer;
                    atr.GIDTyp = _gidTyp;
                    Plugin.AtrybutDodaj(idSesji, atr, _connStr);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Wystąpił błąd: {ex.Message}, {ex.InnerException}");
            }
            finally
            {
                if (idSesji != 0)
                {
                    XLWyloguj(idSesji);
                }
            }
        }

        private void PokazInfoIZakonczPrace(WynikWysylki wynikWysylki)
        {
            MessageBox.Show($"Przesyłka została zapisana w systemie spedytora. Numer przesyłki: {wynikWysylki.NumerWysylki}");
            PokazPDFListuPrzewozowego(wynikWysylki.NumerWysylki, wynikWysylki.Pdf);
            Environment.Exit((int)ExitCode.Success);
        }

        private int XlZaloguj()
        {
            return Plugin.SesjaXLUtworz(new Guid(), _ope, _pass, _baza);
        }

        private void XLWyloguj(int idSesji)
        {
            Plugin.SesjaXLZakoncz(idSesji);
        }

        private void btnFedex_Click(object sender, EventArgs e)
        {
            var nc = new NetworkCredential("", ConfigurationManager.AppSettings["SpedytorFedexApiKey"]);

            DanePrzesylki dp = UstawDaneWysylki();
            dp.Nadawca.Id = ConfigurationManager.AppSettings["SpedytorFedexIdKlientaNadawcy"];
            
            var wynikWysylki = Helpers.GenerujDaneWysylkiZPytaniemOParametryWin(Enums.TypPrzesylki.FedexPrzesylkaPaczka, dp, nc);

            if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.Ok)
            {
                UstawAtr(wynikWysylki.NumerWysylki);
                PokazInfoIZakonczPrace(wynikWysylki);
            }
            else if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.CancelByUser)
            {
                MessageBox.Show(@"Anulowano");
            }
            else
            {
                MessageBox.Show(String.Format("Wystąpił błąd o treści: {0}, Kod błędu: {1}", wynikWysylki.Info,
                    wynikWysylki));
            }
        }

        private void btnPocztaPolska_Click(object sender, EventArgs e)
        {
            var nc = new NetworkCredential(
                ConfigurationManager.AppSettings["SpedytorPocztaPolskaUzytkownik"],
                ConfigurationManager.AppSettings["SpedytorPocztaPolskaHaslo"],
                ConfigurationManager.AppSettings["SpedytorPocztaPolskaApiAdres"]
            );

            DanePrzesylki dp = UstawDaneWysylki();
            

            var wynikWysylki = Helpers.GenerujDaneWysylkiZPytaniemOParametryWin(Enums.TypPrzesylki.PocztaPolskaPaczka, dp, nc);

            if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.Ok)
            {
                UstawAtr(wynikWysylki.NumerWysylki);
                PokazInfoIZakonczPrace(wynikWysylki);
            }
            else if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.CancelByUser)
            {
                MessageBox.Show(@"Anulowano");
            }
            else
            {
                MessageBox.Show(String.Format("Wystąpił błąd o treści: {0}, Kod błędu: {1}", wynikWysylki.Info,
                    wynikWysylki));
            }
        }

        private void btnUps_Click(object sender, EventArgs e)
        {
            var nc = new NetworkCredential(
                    ConfigurationManager.AppSettings["SpedytorUPSUzytkownik"],
                    ConfigurationManager.AppSettings["SpedytorUPSHaslo"],
                    ConfigurationManager.AppSettings["SpedytorUPSApiKey"]
                    );

            DanePrzesylki dp = UstawDaneWysylki();
            var wynikWysylki = Helpers.GenerujDaneWysylkiZPytaniemOParametryWin(Enums.TypPrzesylki.UpsPaczka, dp, nc);

            if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.Ok)
            {
                UstawAtr(wynikWysylki.NumerWysylki);
                PokazInfoIZakonczPrace(wynikWysylki);
            }
            else if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.CancelByUser)
            {
                MessageBox.Show(@"Anulowano");
            }
            else
            {
                MessageBox.Show(String.Format("Wystąpił błąd o treści: {0}, Kod błędu: {1}", wynikWysylki.Info,
                    wynikWysylki));
            }
        }

        private void btnDPD_Click(object sender, EventArgs e)
        {
        
            var nc = new NetworkCredential(
                ConfigurationManager.AppSettings["SpedytorDPDUzytkownik"],
                ConfigurationManager.AppSettings["SpedytorDPDHaslo"],
                ConfigurationManager.AppSettings["SpedytorDPDApiAdres"]
            );

            DanePrzesylki dp = UstawDaneWysylki();
            dp.Nadawca.NumerKlienta = Int32.Parse(ConfigurationManager.AppSettings["SpedytorDPDFID"]);

            var wynikWysylki = Helpers.GenerujDaneWysylkiZPytaniemOParametryWin(Enums.TypPrzesylki.DpdPaczka, dp, nc);

            if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.Ok)
            {
                UstawAtr(wynikWysylki.NumerWysylki);
                DbHelpers.InsertPackage(wynikWysylki, "DPD", _gidTyp, _gidNumer);
                PokazInfoIZakonczPrace(wynikWysylki);
            }
            else if (wynikWysylki.KodWyjscia == Enums.KodWyjscia.CancelByUser)
            {
                MessageBox.Show(@"Anulowano");
            }
            else
            {
                MessageBox.Show(String.Format("Wystąpił błąd o treści: {0}, Kod błędu: {1}", wynikWysylki.Info,
                    wynikWysylki));
            }
        }

        private void Spedytor_Shown(object sender, EventArgs e)
        {
            if (!DbHelpers.CheckIfTableExists())
            {
                DbHelpers.CreateTableForSpedition();
            }
        }

        private void dpdProtokolPrzekazania_Click(object sender, EventArgs e)
        {
            var nrListow = DbHelpers.GetPackageId("DPD");

            if (nrListow.Count > 0)
            {
                var nc = new NetworkCredential(
                    ConfigurationManager.AppSettings["SpedytorDPDUzytkownik"],
                    ConfigurationManager.AppSettings["SpedytorDPDHaslo"],
                    ConfigurationManager.AppSettings["SpedytorDPDApiAdres"]
                );

                var nadawca = new DaneKontrahenta()
                {
                    NazwaFirmy = ConfigurationManager.AppSettings["NazwaFirmy"],
                    NumerKonta = ConfigurationManager.AppSettings["NumerKontaBankowego"],
                    KodKraju = "PL",
                    KodPocztowy = ConfigurationManager.AppSettings["KodPocztowy"],
                    Miasto = ConfigurationManager.AppSettings["Miasto"],
                    Ulica = ConfigurationManager.AppSettings["Ulica"],
                    NumerDomu = ConfigurationManager.AppSettings["NumerDomu"],
                    Nazwisko = ConfigurationManager.AppSettings["Nazwisko"],
                    NumerKlienta = int.Parse(ConfigurationManager.AppSettings["SpedytorDPDFID"])
                };

                var x = WysylkaDPD.GenerujProtokolPrzekazania(nrListow, nadawca, nc);

                GenerujEtykiete("DPDProtokol", x.DocumentId, "PDF", x.Pdf);

                if (!x.DocumentId.Equals("0"))
                    DbHelpers.UpdateListNumbers("DPD");
            }
            else
            {
                MessageBox.Show(@"Brak zapisanych paczek w bazie. Kończę pracę!");
            }
            Close();
        }

        private void GenerujEtykiete(string nazwaSpedytora, string nrPrzesylki, string typPliku, byte[] bytes)
        {
            try
            {
                string fileName = string.Format("{0}_{1}_({2}_{3}).{4}", nazwaSpedytora, nrPrzesylki, DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), typPliku).Replace(" ", "_").Replace(":", "");
                //string fileFullPath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), fileName);
                string fileFullPath = Path.Combine(GlobalStorage.FolderDokumentow, fileName);
                //string fileFullPath = Path.Combine(GlobalStorage.FolderDokumentow, fileName);

                using (var fileStream = new FileStream(fileFullPath, FileMode.OpenOrCreate))
                {
                    fileStream.Write(bytes, 0, bytes.Length);
                }

                System.Diagnostics.Process.Start($"{fileFullPath}");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Spedytor_Load(object sender, EventArgs e)
        {

        }
    }
}

